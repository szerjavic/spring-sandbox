create table if not exists employees (
  id  identity,
  firstName varchar(255) not null,
  lastName varchar(255) not null,
  username varchar(255) not null,
  createdAt timestamp not null,
  updatedAt timestamp
  );