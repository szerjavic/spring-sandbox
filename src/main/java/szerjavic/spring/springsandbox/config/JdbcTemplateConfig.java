package szerjavic.spring.springsandbox.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
public class JdbcTemplateConfig {

    public static final String EMBEDDED_H2_TEMPLATE = "h2EmbeddedTemplate";
    private static final String EMBEDDED_H2_DATASOURCE = "h2EmbeddedDs";

    @Bean(name = EMBEDDED_H2_DATASOURCE)
    @Profile("dev")
    public DataSource getEmbeddedDs() {

        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        builder.setType(EmbeddedDatabaseType.H2);
        builder.addScript("employees.sql");

        return builder.build();
    }

    @Bean(name = EMBEDDED_H2_DATASOURCE)
    @Profile("prod")
    public DataSource getEmbeddedDsProd() {

        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        builder.setType(EmbeddedDatabaseType.H2);
        builder.addScript("employees.sql");

        return builder.build();
    }

    @Bean(name = EMBEDDED_H2_TEMPLATE)
    public JdbcTemplate h2JdbcTemplate(@Qualifier(EMBEDDED_H2_DATASOURCE) DataSource dataSource) {

        return new JdbcTemplate(dataSource);
    }

    @Bean
    public DataSourceTransactionManager transactionManager(@Qualifier(EMBEDDED_H2_DATASOURCE) DataSource dataSource) {

        return new DataSourceTransactionManager(dataSource);
    }

}
