package szerjavic.spring.springsandbox.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@EnableJpaRepositories(
        basePackages = "szerjavic.spring.springsandbox.core.jpa",
        entityManagerFactoryRef = "jpaEntityManager",
        transactionManagerRef = "jpaTransactionManager"
)
public class JpaRepoConfig {

    @Bean
    @Primary
    public LocalContainerEntityManagerFactoryBean jpaEntityManager() {

        LocalContainerEntityManagerFactoryBean em
                = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(userDataSource());
        em.setPackagesToScan(
                new String[]{"szerjavic.spring.springsandbox.core.jpa.model"});
        
        HibernateJpaVendorAdapter vendorAdapter
                = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);

        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto",
                "create-drop");
        properties.put("hibernate.dialect",
                "org.hibernate.dialect.H2Dialect");
        em.setJpaPropertyMap(properties);

        return em;

    }

    @Bean
    @Primary
    public DataSource userDataSource() {

        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        builder.setName("jpaDb");
        builder.setType(EmbeddedDatabaseType.H2);
        EmbeddedDatabase db = builder.build();

        return db;
    }

    @Primary
    @Bean
    public PlatformTransactionManager jpaTransactionManager() {

        JpaTransactionManager transactionManager
                = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(
                jpaEntityManager().getObject());
        return transactionManager;
    }
}
