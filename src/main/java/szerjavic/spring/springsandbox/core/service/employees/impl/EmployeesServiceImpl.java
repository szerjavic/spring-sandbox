package szerjavic.spring.springsandbox.core.service.employees.impl;

import org.springframework.stereotype.Service;
import szerjavic.spring.springsandbox.core.jdbctemplate.model.Employee;
import szerjavic.spring.springsandbox.core.jdbctemplate.repository.EmployeeRepository;
import szerjavic.spring.springsandbox.core.service.employees.EmployeesService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Service
public class EmployeesServiceImpl implements EmployeesService {


    private EmployeeRepository employeeRepository;

    public EmployeesServiceImpl(EmployeeRepository employeeRepository) {

        this.employeeRepository = employeeRepository;
    }


    @Override
    public List<Employee> getEmployees() {

        Collection<Employee> employeeCollection = employeeRepository.findAllEmployee();
        List<Employee> employeeList = new ArrayList<>(employeeCollection);
        return employeeList;
    }
}
