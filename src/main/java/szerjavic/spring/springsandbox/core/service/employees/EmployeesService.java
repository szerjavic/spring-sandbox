package szerjavic.spring.springsandbox.core.service.employees;

import szerjavic.spring.springsandbox.core.jdbctemplate.model.Employee;

import java.util.List;

public interface EmployeesService {

    List<Employee> getEmployees();
}
