package szerjavic.spring.springsandbox.core.jpa.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import szerjavic.spring.springsandbox.core.jpa.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String firstName);
}
