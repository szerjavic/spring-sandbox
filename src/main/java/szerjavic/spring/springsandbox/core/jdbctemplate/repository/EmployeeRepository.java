package szerjavic.spring.springsandbox.core.jdbctemplate.repository;

import szerjavic.spring.springsandbox.core.jdbctemplate.model.Employee;

import java.util.Collection;
import java.util.Optional;

public interface EmployeeRepository {

    Optional<Employee> saveEmployee(Employee employee);

    Collection<Employee> findAllEmployee();

    Optional<Employee> findEmployeeByUsername(String firstName);
}
