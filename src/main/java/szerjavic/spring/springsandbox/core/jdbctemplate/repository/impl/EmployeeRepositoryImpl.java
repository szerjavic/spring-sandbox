package szerjavic.spring.springsandbox.core.jdbctemplate.repository.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import szerjavic.spring.springsandbox.core.jdbctemplate.model.Employee;
import szerjavic.spring.springsandbox.core.jdbctemplate.repository.EmployeeRepository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static szerjavic.spring.springsandbox.config.JdbcTemplateConfig.EMBEDDED_H2_TEMPLATE;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {


    private JdbcTemplate jdbc;

    public EmployeeRepositoryImpl(@Qualifier(EMBEDDED_H2_TEMPLATE) JdbcTemplate jdbc) {

        this.jdbc = jdbc;
    }

    @Override
    public Optional<Employee> saveEmployee(Employee employee) {

        //TODO refactor,add updated case

        LocalDateTime created = employee.getCreatedAt() == null ? LocalDateTime.now() : employee.getCreatedAt();
        LocalDateTime updatedAt = employee.getId() == null ? null : LocalDateTime.now();

        List args = Arrays.asList(
                employee.getFirstName(),
                employee.getLastName(),
                employee.getUsername(),
                convertLocalDatetimeToTimestamp(created),
                convertLocalDatetimeToTimestamp(updatedAt)
        );

        PreparedStatementCreatorFactory pscFactory =
                new PreparedStatementCreatorFactory(
                        "insert into employees (firstName,lastName,username,createdAt,updatedAt) values (?,?,?,?,?)",
                        Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP
                );
        pscFactory.setReturnGeneratedKeys(true);
        PreparedStatementCreator psc = pscFactory.newPreparedStatementCreator(
                args);
        KeyHolder keyHolder = new GeneratedKeyHolder();


        int i = jdbc.update(psc, keyHolder);

        if (i != 1) {
            throw new IllegalArgumentException();
        }

        if (employee.getId() == null) {
            employee.setId(keyHolder.getKey().longValue());
            employee.setCreatedAt(created);
        } else {
            employee.setUpdatedAt(updatedAt);
        }

        return Optional.of(employee);
    }

    private Timestamp convertLocalDatetimeToTimestamp(LocalDateTime localDateTime) {

        return localDateTime == null ? null : Timestamp.valueOf(localDateTime);

    }

    @Override
    public Collection<Employee> findAllEmployee() {

        return jdbc.query("select * from employees", this::mapRowToEmployee);
    }

    private Employee mapRowToEmployee(ResultSet resultSet, int i) throws SQLException {

        Employee employee = new Employee();
        employee.setId(resultSet.getLong("id"));
        employee.setFirstName(resultSet.getString("firstName"));
        employee.setLastName(resultSet.getString("lastName"));
        employee.setUsername(resultSet.getString("username"));

        return employee;

    }

    @Override
    public Optional<Employee> findEmployeeByUsername(String firstName) {

        Employee employee = jdbc.queryForObject("select * from employees where username=?", this::mapRowToEmployee, firstName);

        return Optional.ofNullable(employee);
    }
}
