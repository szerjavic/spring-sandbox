package szerjavic.spring.springsandbox.core.repository;

import szerjavic.spring.springsandbox.core.model.Employee;

import java.util.Collection;
import java.util.Optional;

public interface EmployeeRepository {

    Optional<Employee> saveEmployee(Employee employee);

    Collection<Employee> findAllEmployee();

    Optional<Employee> findEmployeeByUsername(String firstName);
}
