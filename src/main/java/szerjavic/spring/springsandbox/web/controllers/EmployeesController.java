package szerjavic.spring.springsandbox.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import szerjavic.spring.springsandbox.core.jdbctemplate.model.Employee;
import szerjavic.spring.springsandbox.core.service.employees.EmployeesService;

import java.util.List;

@Controller
@RequestMapping("/employees")
public class EmployeesController {

    private EmployeesService employeesService;

    public EmployeesController(EmployeesService employeesService) {
        this.employeesService = employeesService;
    }

    @GetMapping
    public String getEmployees(Model model){

        List<Employee> employeeList = employeesService.getEmployees();

        model.addAttribute("employees",employeeList);

        return "employees";

    }
}
