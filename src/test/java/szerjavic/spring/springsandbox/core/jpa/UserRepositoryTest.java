package szerjavic.spring.springsandbox.core.jpa;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import szerjavic.spring.springsandbox.core.jpa.model.User;
import szerjavic.spring.springsandbox.core.jpa.model.repository.UserRepository;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class UserRepositoryTest {

    private final String USERNAME = "username";
    private final String FIRST_NAME = "firstName";
    @Autowired
    private UserRepository userRepository;


    @Test
    public void testSaveFetch() {

        User user = userRepository.save(new User(USERNAME, FIRST_NAME, "lastName", LocalDateTime.now(), null));

        assertThat(user.getId()).isNotNull();

        User userFromFetch = userRepository.findByUsername(USERNAME);

        assertThat(userFromFetch.getId()).isNotNull();

        assertThat(user.getId()).isEqualTo(userFromFetch.getId());
    }
}