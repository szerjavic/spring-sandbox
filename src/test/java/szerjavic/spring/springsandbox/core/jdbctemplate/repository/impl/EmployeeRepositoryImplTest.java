package szerjavic.spring.springsandbox.core.jdbctemplate.repository.impl;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import szerjavic.spring.springsandbox.core.jdbctemplate.model.Employee;
import szerjavic.spring.springsandbox.core.jdbctemplate.repository.EmployeeRepository;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class EmployeeRepositoryImplTest {

    private final String FIRST_NAME = "Meho";
    private final String USERNAME = "meho123";

    @Autowired
    private EmployeeRepository employeeRepository;

    private Employee getEmployee() {

        Employee employee = new Employee();
        employee.setFirstName(FIRST_NAME);
        employee.setLastName("LastName");
        employee.setUsername(USERNAME);
        return employee;
    }

    @Test
    public void saveEmployee() {

        Employee employee = getEmployee();
        employee.setCreatedAt(LocalDateTime.now());

        Optional<Employee> employeeOptional = employeeRepository.saveEmployee(employee);
        assertThat(employeeOptional.get()).isNotNull();
        assertThat(employeeOptional.get().getCreatedAt()).isNotNull();

    }


    @Ignore
    public void findAllEmployee() {


    }

    @Test(expected = IncorrectResultSizeDataAccessException.class)
    public void incorrectResultSize() {

        Employee employee = getEmployee();

        Optional<Employee> employeeOptional = employeeRepository.saveEmployee(employee);
        Optional<Employee> employeeOptional2 = employeeRepository.saveEmployee(employee);


        assertThat(employeeOptional.get()).isNotNull();
        assertThat(employeeOptional2.get()).isNotNull();

        Optional<Employee> optional = employeeRepository.findEmployeeByUsername(USERNAME);

        assertThat(optional.get().getFirstName()).isEqualTo(FIRST_NAME);

    }

    @Test
    public void findEmployeeByUsername() {

        Employee employee = getEmployee();
        employee.setCreatedAt(LocalDateTime.now());

        Optional<Employee> employeeOptional = employeeRepository.saveEmployee(employee);


        assertThat(employeeOptional.get()).isNotNull();

        Optional<Employee> optional = employeeRepository.findEmployeeByUsername(USERNAME);

        assertThat(optional.get().getFirstName()).isEqualTo(FIRST_NAME);

    }
}